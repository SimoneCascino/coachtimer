package it.simonecascino.coachtimer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import it.simonecascino.coachtimer.data.network.utils.ApiStatus
import it.simonecascino.coachtimer.domain.models.Player
import it.simonecascino.coachtimer.domain.repositories.IPlayersRepository
import kotlinx.coroutines.delay

class FakePlayersRepository(private val resultApiStatus: ApiStatus, private val resultPlayers: List<Player>): IPlayersRepository {

    private val _apiStatus = MutableLiveData<ApiStatus>()
    private val _users = MutableLiveData<List<Player>>()

    override val playerApiStatus: LiveData<ApiStatus>
        get() = _apiStatus

    override val players: LiveData<List<Player>>
        get() = _users

    override suspend fun getUsers() {

        delay(2000)

        _users.postValue(resultPlayers)

        _apiStatus.postValue(resultApiStatus)

    }


}