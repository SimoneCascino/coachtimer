package it.simonecascino.coachtimer.data.network.utils

import android.os.Build
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import it.simonecascino.coachtimer.getOrAwaitValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import retrofit2.Response
import java.io.IOException

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P])
class UtilitiesKtTest{

    val liveApiStatus = MutableLiveData<ApiStatus>()

    val successfulApi: () -> Response<Any> = {
        Response.success(Any())
    }

    val exceptionApi: () -> Response<Any> = {
        throw IOException()
    }

    @Test
    fun `test whether apistatus is success after a successful api call`(){

        handleApiWithStatus(liveApiStatus) { successfulApi() }

        val apiStatus = liveApiStatus.getOrAwaitValue()

        assertThat(apiStatus, `is`(ApiStatus.Success))

    }

    @Test
    fun `test whether apistatus handle the error after a fail`(){

        handleApiWithStatus(liveApiStatus) { exceptionApi() }

        val apiStatus = liveApiStatus.getOrAwaitValue()

        assertThat(apiStatus, `is`(ApiStatus.Fail))

    }


}