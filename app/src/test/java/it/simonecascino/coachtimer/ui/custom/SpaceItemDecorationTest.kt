package it.simonecascino.coachtimer.ui.custom

import android.graphics.Rect
import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import it.simonecascino.coachtimer.presenttion.custom.SpaceItemDecoration
import org.hamcrest.CoreMatchers
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.jvm.isAccessible

private const val TEST_DP = 2

@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class SpaceItemDecorationTest{

    @Test
    fun `decoration calculate the right offset for first position`(){

        val outRect = Rect()
        val size = 10
        val spanCount = 2
        val position = 0

        SpaceItemDecoration::class.declaredMemberFunctions.find {
            it.name == "addOffset"
        }?.let {
            it.isAccessible = true
            it.call(SpaceItemDecoration(TEST_DP), outRect, size, spanCount, position)
        }

        assertThat(outRect.left, CoreMatchers.`is`(TEST_DP))
        assertThat(outRect.top, CoreMatchers.`is`(TEST_DP))
        assertThat(outRect.right, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.bottom, CoreMatchers.`is`(TEST_DP / 2))

    }

    @Test
    fun `decoration calculate the right offset for last position`(){

        val outRect = Rect()
        val size = 10
        val spanCount = 2
        val position = 9

        SpaceItemDecoration::class.declaredMemberFunctions.find {
            it.name == "addOffset"
        }?.let {
            it.isAccessible = true
            it.call(SpaceItemDecoration(TEST_DP), outRect, size, spanCount, position)
        }

        assertThat(outRect.left, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.top, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.right, CoreMatchers.`is`(TEST_DP))
        assertThat(outRect.bottom, CoreMatchers.`is`(TEST_DP))

    }

    @Test
    fun `decoration calculate the right offset for an intermediate position`(){

        val outRect = Rect()
        val size = 10
        val spanCount = 2
        val position = 4

        SpaceItemDecoration::class.declaredMemberFunctions.find {
            it.name == "addOffset"
        }?.let {
            it.isAccessible = true
            it.call(SpaceItemDecoration(TEST_DP), outRect, size, spanCount, position)
        }

        assertThat(outRect.left, CoreMatchers.`is`(TEST_DP))
        assertThat(outRect.top, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.right, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.bottom, CoreMatchers.`is`(TEST_DP / 2))

    }

    @Test
    fun `decoration calculate the right offset if the item is surrounded`(){

        val outRect = Rect()
        val size = 10
        val spanCount = 3
        val position = 4

        SpaceItemDecoration::class.declaredMemberFunctions.find {
            it.name == "addOffset"
        }?.let {
            it.isAccessible = true
            it.call(SpaceItemDecoration(TEST_DP), outRect, size, spanCount, position)
        }

        assertThat(outRect.left, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.top, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.right, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.bottom, CoreMatchers.`is`(TEST_DP / 2))

    }

    @Test
    fun `decoration calculate the right offset with an odd size`(){

        val outRect = Rect()
        val size = 9
        val spanCount = 2
        val position = 6

        SpaceItemDecoration::class.declaredMemberFunctions.find {
            it.name == "addOffset"
        }?.let {
            it.isAccessible = true
            it.call(SpaceItemDecoration(TEST_DP), outRect, size, spanCount, position)
        }

        assertThat(outRect.left, CoreMatchers.`is`(TEST_DP))
        assertThat(outRect.top, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.right, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.bottom, CoreMatchers.`is`(TEST_DP / 2))

    }

    @Test
    fun `decoration calculate the right offset with an odd size and another position`(){

        val outRect = Rect()
        val size = 9
        val spanCount = 2
        val position = 7

        SpaceItemDecoration::class.declaredMemberFunctions.find {
            it.name == "addOffset"
        }?.let {
            it.isAccessible = true
            it.call(SpaceItemDecoration(TEST_DP), outRect, size, spanCount, position)
        }

        assertThat(outRect.left, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.top, CoreMatchers.`is`(TEST_DP / 2))
        assertThat(outRect.right, CoreMatchers.`is`(TEST_DP))
        assertThat(outRect.bottom, CoreMatchers.`is`(TEST_DP / 2))

    }

}