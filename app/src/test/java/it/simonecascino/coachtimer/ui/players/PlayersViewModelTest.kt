package it.simonecascino.coachtimer.ui.players

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import it.simonecascino.coachtimer.FakePlayersRepository
import it.simonecascino.coachtimer.data.network.utils.ApiStatus
import it.simonecascino.coachtimer.domain.models.Player
import it.simonecascino.coachtimer.presenttion.ui.players.PlayersViewModel
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
@Config(application = HiltTestApplication::class, sdk = [Build.VERSION_CODES.P])
class PlayersViewModelTest{

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun `test api status when viewModel is instantiated`(){

        val expectedApiStatus = ApiStatus.Success

        val viewModel = PlayersViewModel(FakePlayersRepository(expectedApiStatus, listOf()))

        val apiStatus = viewModel.playerApiStatus.getOrAwaitValue(time = 10)

        assertThat(apiStatus, `is`(ApiStatus.Success))

    }

    @Test
    fun `test users when viewModel is instantiated`(){

        val usersResult = listOf(
            Player("Mr", "Simone", "Cascino", "", "", ""),
            Player("Mr", "John", "Doe", "", "", ""),
        )

        val viewModel = PlayersViewModel(FakePlayersRepository(ApiStatus.Success, usersResult))

        val users = viewModel.players.getOrAwaitValue(time = 10)

        assertThat(users, `is`(usersResult))

    }

}