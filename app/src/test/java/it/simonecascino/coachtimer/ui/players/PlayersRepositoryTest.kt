package it.simonecascino.coachtimer.ui.players

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import it.simonecascino.coachtimer.data.repositories.PlayersRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
@Config(application = HiltTestApplication::class, sdk = [Build.VERSION_CODES.P])
class PlayersRepositoryTest{

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var playerRepository: PlayersRepository

    @Before
    fun init() {
        hiltRule.inject()
    }

    @Test
    fun `test get user api`(){

        val apiStatus = playerRepository.playerApiStatus.getOrAwaitValue (time = 10){

        }
    }

}