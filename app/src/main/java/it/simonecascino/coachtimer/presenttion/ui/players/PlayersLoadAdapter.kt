package it.simonecascino.coachtimer.presenttion.ui.players

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import it.simonecascino.coachtimer.R
import it.simonecascino.coachtimer.databinding.ItemLoadPlayerBinding

class PlayersLoadAdapter(private val retry: () -> Unit):  LoadStateAdapter<PlayersLoadAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder, loadState: LoadState) {
        holder.bind(loadState, retry)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState) =
        ViewHolder.from(parent)

    class ViewHolder private constructor(private val binding: ItemLoadPlayerBinding):
        RecyclerView.ViewHolder(binding.root){

        fun bind(loadState: LoadState, retry: () -> Unit){

            binding.progressBar.visibility = if(loadState == LoadState.Loading) View.VISIBLE else View.GONE
            binding.retryButton.visibility = if(loadState is LoadState.Error) View.VISIBLE else View.GONE
            binding.errorText.visibility = binding.retryButton.visibility

            binding.retryButton.setOnClickListener { retry() }

        }

        companion object{
            fun from(parent: ViewGroup) = ViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_load_player, parent, false))
        }

    }

}