package it.simonecascino.coachtimer.presenttion.utils

import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import it.simonecascino.coachtimer.R
import it.simonecascino.coachtimer.domain.models.Lap
import it.simonecascino.coachtimer.presenttion.ui.session.LapsAdapter

@BindingAdapter("simpleLoad")
fun ImageView.simpleLoad(url: String){
    Glide.with(this).load(url).into(this)
}

@BindingAdapter("loadProfile")
fun ImageView.loadProfile(url: String){
    Glide.with(this).load(url).apply(RequestOptions.circleCropTransform()).into(this)
}

@BindingAdapter("changeVisibility")
fun View.changeVisibility(isStarted: Boolean){

    if(isStarted && visibility != View.VISIBLE){

        val anim = AnimationUtils.loadAnimation(context, R.anim.pop_in)

        visibility = View.VISIBLE

        startAnimation(anim)

    }

    else if(!isStarted && visibility == View.VISIBLE ){

        val anim = AnimationUtils.loadAnimation(context, R.anim.scale_out)

        anim.setAnimationListener(
            object : AnimationListenerHelper(){

                override fun onAnimationEnd(p0: Animation?) {
                    super.onAnimationEnd(p0)
                    visibility = View.INVISIBLE
                }
            }
        )

        startAnimation(anim)

    }

}

@BindingAdapter("stopwatchIcon")
fun FloatingActionButton.setStopwatchIcon(isStarted: Boolean){

    setImageResource(
        if(isStarted)R.drawable.ic_pause
        else R.drawable.ic_play
    )

}

@BindingAdapter("lapList")
fun RecyclerView.setLapList(values: List<Lap>?){

    values?.let {

        (adapter as? LapsAdapter)?.submitList(it)

    }

}