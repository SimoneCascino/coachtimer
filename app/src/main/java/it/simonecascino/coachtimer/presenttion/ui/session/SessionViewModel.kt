package it.simonecascino.coachtimer.presenttion.ui.session

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.simonecascino.coachtimer.domain.models.Player
import it.simonecascino.coachtimer.domain.usecases.GetSessionUseCases
import kotlinx.coroutines.launch

class SessionViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    private val sessionUseCases: GetSessionUseCases
): ViewModel(){

    private var firstTimeStart = true

    val player = savedStateHandle.get<Player>("Player")

    val meters = savedStateHandle.get<Int>("meters")

    val laps = sessionUseCases.laps

    val liveTimer = sessionUseCases.liveTimer

    val isStarted = sessionUseCases.isStarted

    fun startStopTimer(){

        if(firstTimeStart){

            firstTimeStart = false

            if(player!=null)viewModelScope.launch {
                sessionUseCases.savePlayer(player)
            }

        }

        if(isStarted.value == true){
            sessionUseCases.stop()
        }

        else sessionUseCases.start()
    }

    fun lap(){

        viewModelScope.launch {
            sessionUseCases.lap()
        }

    }

    override fun onCleared() {
        super.onCleared()
        sessionUseCases.destroy()
    }

}