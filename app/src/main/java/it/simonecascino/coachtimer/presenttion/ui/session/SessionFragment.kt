package it.simonecascino.coachtimer.presenttion.ui.session

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import dagger.hilt.android.AndroidEntryPoint
import it.simonecascino.coachtimer.R
import it.simonecascino.coachtimer.databinding.FragmentSessionBinding

@AndroidEntryPoint
class SessionFragment : Fragment() {

    private lateinit var binding: FragmentSessionBinding

    private val args: SessionFragmentArgs by navArgs()

    private val viewModel: SessionViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_session, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        NavigationUI.setupWithNavController(binding.collapsingToolbarLayout, binding.toolbar, navController, appBarConfiguration)

        view.postDelayed({
            binding.fab.visibility = View.VISIBLE
            binding.fab.startAnimation(AnimationUtils.loadAnimation(requireActivity(),
                R.anim.trans_enter_from_bottom))
        }, 500)

        binding.lapList.adapter = LapsAdapter()

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

    }

}