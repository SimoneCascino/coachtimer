package it.simonecascino.coachtimer.presenttion.ui.session

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import it.simonecascino.coachtimer.R
import it.simonecascino.coachtimer.databinding.ItemLapBinding
import it.simonecascino.coachtimer.domain.models.Lap

class LapsAdapter: ListAdapter<Lap, LapsAdapter.ViewHolder>(LapDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.from(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ViewHolder(private val binding: ItemLapBinding): RecyclerView.ViewHolder(binding.root){

        companion object{
            fun from(parent: ViewGroup) = ViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_lap, parent, false
                )
            )
        }

        fun bind(lap: Lap){
            binding.lap = lap
            binding.executePendingBindings()
        }

    }

    private class LapDiff: DiffUtil.ItemCallback<Lap>(){
        override fun areItemsTheSame(oldItem: Lap, newItem: Lap) = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Lap, newItem: Lap) = oldItem == newItem
    }
}