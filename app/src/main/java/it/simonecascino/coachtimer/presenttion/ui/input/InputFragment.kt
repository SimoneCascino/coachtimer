package it.simonecascino.coachtimer.presenttion.ui.input

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import it.simonecascino.coachtimer.R
import it.simonecascino.coachtimer.data.preferences.ILocalPreferences
import it.simonecascino.coachtimer.databinding.FragmentInputBinding
import it.simonecascino.coachtimer.presenttion.utils.navigateSafe
import javax.inject.Inject

@AndroidEntryPoint
class InputFragment: DialogFragment() {

    private val args: InputFragmentArgs by navArgs()

    @Inject lateinit var localPreferences: ILocalPreferences

    private lateinit var binding: FragmentInputBinding

    override fun onResume() {
        super.onResume()

        val alertDialog = dialog as AlertDialog?

        val okButton: Button = alertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE)

        okButton.setOnClickListener {

            val meters = binding.inputField.text.toString()

            val value = runCatching { meters.toInt() }

            if(value.isFailure || value.getOrNull()!! < 1)
                binding.inputLayout.error = getString(R.string.input_error_no_int)

            else findNavController().navigateSafe(R.id.inputFragment,
                InputFragmentDirections.actionInputFragmentToSessionFragment(args.player, value.getOrNull()!!)
            )

        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireActivity()).apply {

            binding = DataBindingUtil.inflate(
                LayoutInflater.from(requireActivity()),
                R.layout.fragment_input,
                null,
                false
            )

            setView(binding.root)

            binding.defaultMeters = localPreferences.getMeters()

            setTitle(args.player.fullName)

            setNegativeButton(R.string.input_negative, null)

            setPositiveButton(R.string.input_positive, null)

        }.create()
    }

}