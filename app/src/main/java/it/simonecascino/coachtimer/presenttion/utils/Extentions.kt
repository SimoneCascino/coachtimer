package it.simonecascino.coachtimer.presenttion.utils

import android.content.Context
import android.util.TypedValue
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import java.text.SimpleDateFormat
import java.util.*

inline fun <T> LifecycleOwner.observe(liveData: LiveData<T>, crossinline observer: (T) -> Unit){

    val observerOwner = if(this is Fragment)
        viewLifecycleOwner else this

    liveData.observe(observerOwner) {
        observer(it)
    }
}

fun Context.dpToPx(dp: Int): Int {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics)
        .toInt()
}

fun Fragment.dpToPx(dp: Int): Int {
    return requireActivity().dpToPx(dp)
}

fun NavController.navigateSafe(@IdRes currentDestinationId: Int, navDirection: NavDirections){
    if (currentDestination?.id == currentDestinationId)
        navigate(navDirection)
}

fun Context.toast(@StringRes stringId: Int){
    Toast.makeText(this, stringId, Toast.LENGTH_SHORT).show()
}

fun Context.toast(text: String){
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}

fun Fragment.toast(text: String){
    requireActivity().toast(text)
}

fun Fragment.toast(@StringRes stringId: Int){
    requireActivity().toast(stringId)
}

fun Date.isoUtcNow(pattern: String = DATE_PATTERN_ISO): String{
    val format = SimpleDateFormat(pattern, Locale.getDefault())
    format.timeZone = TimeZone.getTimeZone("utc")
    return format.format(this)
}