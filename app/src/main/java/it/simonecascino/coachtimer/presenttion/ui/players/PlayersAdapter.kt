package it.simonecascino.coachtimer.presenttion.ui.players

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import it.simonecascino.coachtimer.R
import it.simonecascino.coachtimer.databinding.ItemPlayerBinding
import it.simonecascino.coachtimer.domain.models.Player

class PlayersAdapter(private val listener: ItemClickListener): PagingDataAdapter<Player, PlayersAdapter.ViewHolder>(
    PlayerDiff()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.from(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    class ViewHolder private constructor(private val binding: ItemPlayerBinding): RecyclerView.ViewHolder(binding.root){

        companion object{
            fun from(parent: ViewGroup) = ViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_player, parent, false))
        }

        fun bind(player: Player?, listener: ItemClickListener){

            player?.let {
                binding.listener = listener
                binding.player = player
                binding.executePendingBindings()
            }
        }

    }

    private class PlayerDiff: DiffUtil.ItemCallback<Player>(){
        override fun areItemsTheSame(oldItem: Player, newItem: Player) = oldItem.fullName != newItem.fullName
        override fun areContentsTheSame(oldItem: Player, newItem: Player) = oldItem == newItem
    }

    class ItemClickListener(private val callback: (player: Player, sharedView: View) -> Unit){

        fun onClick(player: Player, sharedView: View){
            callback(player, sharedView)
        }

    }

}