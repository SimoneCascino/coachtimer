package it.simonecascino.coachtimer.presenttion.utils

import android.view.animation.Animation

abstract class AnimationListenerHelper: Animation.AnimationListener {

    override fun onAnimationStart(p0: Animation?) {

    }

    override fun onAnimationEnd(p0: Animation?) {

    }

    override fun onAnimationRepeat(p0: Animation?) {

    }
}