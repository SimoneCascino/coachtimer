package it.simonecascino.coachtimer.presenttion.ui.players

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import it.simonecascino.coachtimer.domain.usecases.GetPlayersUseCase

class PlayersViewModel @ViewModelInject constructor(playersUseCase: GetPlayersUseCase): ViewModel() {
    val pagingData = playersUseCase.playersStream.cachedIn(viewModelScope)
}