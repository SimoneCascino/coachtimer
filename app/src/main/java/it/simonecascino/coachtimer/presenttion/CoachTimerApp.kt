package it.simonecascino.coachtimer.presenttion

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoachTimerApp: Application()