package it.simonecascino.coachtimer.presenttion.ui.players

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import it.simonecascino.coachtimer.R
import it.simonecascino.coachtimer.databinding.FragmentPlayersBinding
import it.simonecascino.coachtimer.presenttion.custom.SpaceItemDecoration
import it.simonecascino.coachtimer.presenttion.utils.dpToPx
import it.simonecascino.coachtimer.presenttion.utils.navigateSafe
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PlayersFragment : Fragment() {

    private val viewModel: PlayersViewModel by viewModels()

    private lateinit var binding: FragmentPlayersBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_players, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = findNavController()
        val appBarConfiguration = AppBarConfiguration(navController.graph)

        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

        binding.usersList.addItemDecoration(SpaceItemDecoration(dpToPx(16)))

        val adapter = PlayersAdapter(PlayersAdapter.ItemClickListener{ player, sharedView ->
            findNavController().navigateSafe(R.id.playersFragment,
                PlayersFragmentDirections.actionPlayersFragmentToInputFragment(player)
            )
        })

        adapter.stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY

        binding.usersList.adapter = adapter.withLoadStateFooter(PlayersLoadAdapter{adapter.retry()})

        lifecycleScope.launch {

            viewModel.pagingData.collectLatest {
                adapter.submitData(it)
            }

        }


    }
}