package it.simonecascino.coachtimer.di

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import it.simonecascino.coachtimer.data.preferences.ILocalPreferences
import it.simonecascino.coachtimer.data.preferences.LocalPreferences
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object PreferencesModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    fun provideLocalPreferences(preferences: SharedPreferences): ILocalPreferences =
        LocalPreferences(preferences)

}