package it.simonecascino.coachtimer.di

import androidx.lifecycle.MutableLiveData
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import it.simonecascino.coachtimer.data.database.AppDatabase
import it.simonecascino.coachtimer.data.database.daos.LapDao
import it.simonecascino.coachtimer.data.database.daos.PlayerDao
import it.simonecascino.coachtimer.data.network.Api
import it.simonecascino.coachtimer.data.repositories.IPlayersRepository
import it.simonecascino.coachtimer.data.repositories.ISessionRepository
import it.simonecascino.coachtimer.data.utils.PlayerPagingSource
import it.simonecascino.coachtimer.data.repositories.PlayersRepository
import it.simonecascino.coachtimer.data.repositories.SessionRepository
import it.simonecascino.coachtimer.data.utils.LapsMediator
import it.simonecascino.coachtimer.data.utils.StopWatch

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Provides
    fun providePlayerPagingSource(api: Api) = PlayerPagingSource(api)

    @Provides
    fun providePlayerRepository(playerPagingSource: PlayerPagingSource): IPlayersRepository {
        return PlayersRepository(playerPagingSource)
    }



    @Provides
    fun provideTimerTextLiveData() = MutableLiveData<String>()

    @Provides
    fun provideBooleanLiveData() = MutableLiveData<Boolean>()

    @Provides
    fun provideStopwatch() = StopWatch()

    @Provides
    fun provideLapsMediator(lapDao: LapDao) = LapsMediator(lapDao)

    @Provides
    fun provideSessionRepository(database: AppDatabase,
                                 lapsMediator: LapsMediator,
                                 liveText: MutableLiveData<String>,
                                 isStarted: MutableLiveData<Boolean>,
                                 stopWatch: StopWatch): ISessionRepository{
        return SessionRepository(database, lapsMediator, liveText, isStarted, stopWatch)
    }

}
