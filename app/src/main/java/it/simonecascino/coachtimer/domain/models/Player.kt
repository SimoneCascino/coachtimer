package it.simonecascino.coachtimer.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Player(
    val title: String,
    val first: String,
    val last: String,
    val large: String,
    val medium: String,
    val thumbnail: String
): Parcelable{

    @IgnoredOnParcel
    val fullName = "$first $last"
}