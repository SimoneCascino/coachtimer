package it.simonecascino.coachtimer.domain.usecases

import it.simonecascino.coachtimer.data.repositories.ISessionRepository
import it.simonecascino.coachtimer.domain.models.Player
import javax.inject.Inject

class GetSessionUseCases @Inject constructor(private val repository: ISessionRepository){

    val liveTimer = repository.liveTimer

    val isStarted = repository.isStarted

    val laps = repository.laps

    suspend fun savePlayer(player: Player){
        repository.savePlayerAndSession(player)
    }

    fun start(){
        repository.start()
    }

    fun stop(){
        repository.stop()
    }

    fun destroy() {
        repository.destroy()
    }

    suspend fun lap(){
        repository.lap()
    }

}