package it.simonecascino.coachtimer.domain.models

data class Lap(
    val id: Long,
    val millis: Long
)