package it.simonecascino.coachtimer.domain.usecases

import it.simonecascino.coachtimer.data.repositories.IPlayersRepository
import javax.inject.Inject

class GetPlayersUseCase @Inject constructor(repository: IPlayersRepository) {
    val playersStream = repository.getPlayersStream()
}