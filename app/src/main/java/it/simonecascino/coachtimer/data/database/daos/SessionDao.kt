package it.simonecascino.coachtimer.data.database.daos

import androidx.room.Dao
import androidx.room.Insert
import it.simonecascino.coachtimer.data.database.entities.SessionDb

@Dao
interface SessionDao {

    @Insert
    fun insertSession(session: SessionDb): Long

}