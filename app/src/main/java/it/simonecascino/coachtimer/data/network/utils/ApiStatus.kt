package it.simonecascino.coachtimer.data.network.utils

import com.squareup.moshi.JsonDataException
import retrofit2.HttpException
import java.net.HttpURLConnection

private const val UNREACHABLE_CONST = -1
private const val JSONPARSE_CONST = -2

sealed class ApiStatus {

    object Success: ApiStatus()
    object Loading: ApiStatus()
    object Idle: ApiStatus()

    class Fail private constructor(val reason: Reason): ApiStatus(){

        companion object{

            fun fromCode(code: Int): Fail{

                val reason = when(code){
                    UNREACHABLE_CONST -> Reason.Unreachable
                    JSONPARSE_CONST -> Reason.Unparsable
                    HttpURLConnection.HTTP_BAD_REQUEST -> Reason.BadRequest
                    HttpURLConnection.HTTP_UNAUTHORIZED -> Reason.Unauthorized
                    HttpURLConnection.HTTP_FORBIDDEN -> Reason.Forbidden
                    HttpURLConnection.HTTP_INTERNAL_ERROR -> Reason.InternalServerError
                    HttpURLConnection.HTTP_NOT_FOUND -> Reason.NotFound
                    else -> Reason.Undefined
                }

                return Fail(reason)

            }

            fun fromException(e: Exception): Fail{

                val code = when (e) {
                    is HttpException -> e.code()
                    is JsonDataException -> JSONPARSE_CONST
                    else -> UNREACHABLE_CONST
                }

                return fromCode(code)

            }

            fun fromReason(reason: Reason) = Fail(reason)

        }

        enum class Reason{
            Unreachable,
            BadRequest,
            Unauthorized,
            Forbidden,
            InternalServerError,
            NotFound,
            Undefined,
            Unparsable
        }

    }

}

typealias Reason = ApiStatus.Fail.Reason