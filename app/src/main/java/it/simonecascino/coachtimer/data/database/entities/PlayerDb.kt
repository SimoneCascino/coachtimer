package it.simonecascino.coachtimer.data.database.entities

import androidx.room.*
import it.simonecascino.coachtimer.domain.models.Player

@Entity(tableName = PlayerDb.TABLE_NAME)
data class PlayerDb (
    @PrimaryKey @ColumnInfo(name = COLUMN_ID) val id: String,
    @ColumnInfo(name = COLUMN_FIRST_NAME) val firstName: String,
    @ColumnInfo(name = COLUMN_LAST_NAME) val lastName: String,

    @Embedded
    val images: ImagesDb

){

    @Ignore val fullName = id

    data class ImagesDb(
        @ColumnInfo(name = COLUMN_LARGE) val large: String,
        @ColumnInfo(name = COLUMN_MEDIUM) val medium: String,
        @ColumnInfo(name = COLUMN_THUMBNAIL) val thumbnail: String
    ){

        companion object{
            const val COLUMN_LARGE = "large"
            const val COLUMN_MEDIUM = "medium"
            const val COLUMN_THUMBNAIL = "thumbnail"
        }

    }

    companion object{

        const val TABLE_NAME = "players"
        const val COLUMN_ID = "id"
        const val COLUMN_FIRST_NAME = "first_name"
        const val COLUMN_LAST_NAME = "last_name"

        fun fromModel(player: Player): PlayerDb{

            return PlayerDb(
                player.fullName,
                player.first,
                player.last,
                ImagesDb(
                    player.large,
                    player.medium,
                    player.thumbnail
                )
            )

        }
    }

}