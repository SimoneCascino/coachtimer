package it.simonecascino.coachtimer.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import it.simonecascino.coachtimer.presenttion.utils.isoUtcNow
import java.util.*

@Entity(tableName = SessionDb.TABLE_NAME,
        foreignKeys = [

            ForeignKey(
                entity = PlayerDb::class,
                parentColumns = [PlayerDb.COLUMN_ID],
                childColumns = [SessionDb.COLUMN_PLAYER_ID]
            )

        ]
    )
data class SessionDb(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = COLUMN_ID) val id: Long = 0,
    @ColumnInfo(name = COLUMN_PLAYER_ID) val playerId: String,
    @ColumnInfo(name = COLUMN_DATE) val date: String
){

    companion object{

        const val TABLE_NAME = "sessions"

        const val COLUMN_ID = "id"
        const val COLUMN_PLAYER_ID = "player_id"
        const val COLUMN_DATE = "date"

        fun create(playerId: String): SessionDb{

            return SessionDb(
                playerId = playerId,
                date = Date().isoUtcNow()
            )

        }

    }

}