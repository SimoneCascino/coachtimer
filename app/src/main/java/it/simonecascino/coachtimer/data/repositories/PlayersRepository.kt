package it.simonecascino.coachtimer.data.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import it.simonecascino.coachtimer.data.utils.PlayerPagingSource
import it.simonecascino.coachtimer.domain.models.Player
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface IPlayersRepository{
    fun getPlayersStream(): Flow<PagingData<Player>>
}

class PlayersRepository @Inject constructor(private val playerPagingSource: PlayerPagingSource): IPlayersRepository {

    override fun getPlayersStream(): Flow<PagingData<Player>> {

        return Pager(
            config = PagingConfig(
                pageSize = PlayerPagingSource.PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { playerPagingSource }
        ).flow
    }

}

