package it.simonecascino.coachtimer.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.room.withTransaction
import it.simonecascino.coachtimer.data.database.AppDatabase
import it.simonecascino.coachtimer.data.database.entities.LapDb
import it.simonecascino.coachtimer.data.database.entities.PlayerDb
import it.simonecascino.coachtimer.data.database.entities.SessionDb
import it.simonecascino.coachtimer.data.utils.LapsMediator
import it.simonecascino.coachtimer.data.utils.StopWatch
import it.simonecascino.coachtimer.domain.models.Lap
import it.simonecascino.coachtimer.domain.models.Player

interface ISessionRepository{

    val liveTimer: LiveData<String>

    val isStarted: LiveData<Boolean>

    val laps: LiveData<List<Lap>>

    fun start()

    fun stop()

    suspend fun lap()

    fun destroy()

    suspend fun savePlayerAndSession(player: Player)

}

class SessionRepository(private val database: AppDatabase,
                        private val lapsMediator: LapsMediator,
                        private val _liveTimer: MutableLiveData<String>,
                        private val _isStarted: MutableLiveData<Boolean>,
                        private val stopWatch: StopWatch): ISessionRepository{

    private var liveSessionId = MutableLiveData(0L)

    override val liveTimer: LiveData<String>
        get() = _liveTimer

    override val isStarted =
        Transformations.distinctUntilChanged(_isStarted)

    override val laps: LiveData<List<Lap>>
        get() = lapsMediator

    override fun start() {
        stopWatch.start()
        _isStarted.value = stopWatch.isStarted
    }

    override fun stop() {
        stopWatch.stop()
        _isStarted.value = stopWatch.isStarted
    }

    override suspend fun lap() {
        database.lapDao().insertLap(LapDb(sessionId = liveSessionId.value ?: 0L, millis = stopWatch.time))
    }

    override fun destroy() {
        stopWatch.destroy()
    }

    override suspend fun savePlayerAndSession(player: Player) {

        val playerDb = PlayerDb.fromModel(player)
        val sessionDb = SessionDb.create(playerDb.id)

        database.withTransaction {
            database.playerDao().insertPlayer(PlayerDb.fromModel(player))
            liveSessionId.postValue(database.sessionDao().insertSession(sessionDb))
        }
    }

    init {
        _liveTimer.value = "00:00.00"

        stopWatch.tick = { timeText ->
            _liveTimer.postValue(timeText)
        }

        lapsMediator.connectToSession(liveSessionId)
    }


}