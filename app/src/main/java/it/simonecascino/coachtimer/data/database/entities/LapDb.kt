package it.simonecascino.coachtimer.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import it.simonecascino.coachtimer.domain.models.Lap

@Entity(tableName = LapDb.TABLE_NAME,
    foreignKeys = [

        ForeignKey(
            entity = SessionDb::class,
            parentColumns = [SessionDb.COLUMN_ID],
            childColumns = [LapDb.COLUMN_SESSION_ID]
        )

    ]
)
data class LapDb (
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = COLUMN_ID) val id: Long = 0,
    @ColumnInfo(name = COLUMN_SESSION_ID) val sessionId: Long,
    @ColumnInfo(name = COLUMN_MILLIS) val millis: Long
){

    companion object{

        const val TABLE_NAME = "laps"

        const val COLUMN_ID = "id"
        const val COLUMN_SESSION_ID = "session_id"
        const val COLUMN_MILLIS = "millis"

    }

    fun toDomainModel() = Lap(id, millis)

}

