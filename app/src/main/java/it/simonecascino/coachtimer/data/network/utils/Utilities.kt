package it.simonecascino.coachtimer.data.network.utils

import androidx.lifecycle.MutableLiveData
import retrofit2.Response

/**
 * this helper function is useful to minimize boilerplate code during an api call.
 * It returns the result or null if the api fails, and the api status is managed by an optional
 * live data which react at all the states of the call
 */
inline fun <T> handleApiWithStatus(status: MutableLiveData<ApiStatus>? = null, call: () -> Response<T>): T? {

    return try {

        status?.postValue(ApiStatus.Loading)

        val response = call()

        if (response.isSuccessful) {
            status?.postValue(ApiStatus.Success)
            response.body()
        } else {
            status?.postValue(ApiStatus.Fail.fromCode(response.code()))
            null
        }

    } catch (e: Exception) {
        e.printStackTrace()
        status?.postValue(ApiStatus.Fail.fromException(e))
        null
    }

}

