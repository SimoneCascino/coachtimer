package it.simonecascino.coachtimer.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import it.simonecascino.coachtimer.data.database.daos.LapDao
import it.simonecascino.coachtimer.data.database.daos.PlayerDao
import it.simonecascino.coachtimer.data.database.daos.SessionDao
import it.simonecascino.coachtimer.data.database.entities.LapDb
import it.simonecascino.coachtimer.data.database.entities.PlayerDb
import it.simonecascino.coachtimer.data.database.entities.SessionDb

@Database(entities = [PlayerDb::class, SessionDb::class, LapDb::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase(){

    abstract fun playerDao(): PlayerDao
    abstract fun sessionDao(): SessionDao
    abstract fun lapDao(): LapDao

    companion object{
        const val DB_NAME = "players.db"
    }

}