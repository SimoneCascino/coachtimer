package it.simonecascino.coachtimer.data.utils

import androidx.paging.PagingSource
import it.simonecascino.coachtimer.data.network.Api
import it.simonecascino.coachtimer.domain.models.Player

class PlayerPagingSource(private val api: Api): PagingSource<Int, Player>(){

    companion object{

        const val PAGE_START_INDEX = 1
        const val PAGE_SIZE = 50

    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Player> {

        val position = params.key ?: PAGE_START_INDEX

        return try{

            val players = api.getPlayers(page = position,
                results = params.loadSize).toDomainModel()

            LoadResult.Page(
                data = players,
                prevKey = if (position == PAGE_START_INDEX) null else position - 1,
                nextKey = if (players.isEmpty()) null else position + 1
            )

        }catch(e: Exception){
            LoadResult.Error(e)
        }

    }

}