package it.simonecascino.coachtimer.data.preferences

import android.content.SharedPreferences

private const val KEY_METERS = "meters"

private const val DEFAULT_METERS = 100

interface ILocalPreferences {

    fun saveMeters(meters: Int)

    fun getMeters(): Int

}

class LocalPreferences(private val preferences: SharedPreferences): ILocalPreferences {

    override fun saveMeters(meters: Int) {
        preferences.edit().putInt(KEY_METERS, meters).apply()
    }

    override fun getMeters() = preferences.getInt(KEY_METERS, DEFAULT_METERS)

}