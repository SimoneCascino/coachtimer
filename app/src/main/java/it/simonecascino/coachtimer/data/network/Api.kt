package it.simonecascino.coachtimer.data.network

import it.simonecascino.coachtimer.data.network.pojos.PlayerApiResponse
import it.simonecascino.coachtimer.data.utils.PlayerPagingSource
import retrofit2.http.GET
import retrofit2.http.Query

interface Api{

    @GET(".")
    suspend fun getPlayers(@Query("seed") seed: String = "empatica",
                           @Query("inc") inc: String = "name,picture",
                           @Query("gender") gender: String = "male",
                           @Query("results") results: Int = PlayerPagingSource.PAGE_SIZE,
                           @Query("page") page: Int = PlayerPagingSource.PAGE_START_INDEX): PlayerApiResponse

}