package it.simonecascino.coachtimer.data.network.pojos

import it.simonecascino.coachtimer.domain.models.Player

data class PlayerResponse (
    val name: Name,
    val picture: Picture
){

    data class Name(
        val title: String,
        val first: String,
        val last: String
    )

    data class Picture(
        val large: String,
        val medium: String,
        val thumbnail: String
    )

    fun toDomainModel() = Player(
        name.title,
        name.first,
        name.last,
        picture.large,
        picture.medium,
        picture.thumbnail
    )

}

data class PlayerApiResponse(
    val results: List<PlayerResponse>
){
    fun toDomainModel() = results.map { it.toDomainModel() }
}