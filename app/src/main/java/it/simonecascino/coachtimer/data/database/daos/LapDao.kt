package it.simonecascino.coachtimer.data.database.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import it.simonecascino.coachtimer.data.database.entities.LapDb

@Dao
interface LapDao {

    @Insert
    suspend fun insertLap(lap: LapDb): Long

    @Query("select * from laps where session_id = :sessionId order by millis desc")
    fun getLapsBySessionId(sessionId: Long): LiveData<List<LapDb>>

}