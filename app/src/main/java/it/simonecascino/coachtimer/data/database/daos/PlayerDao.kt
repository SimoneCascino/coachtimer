package it.simonecascino.coachtimer.data.database.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import it.simonecascino.coachtimer.data.database.entities.PlayerDb

@Dao
interface PlayerDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPlayer(player:PlayerDb): Long

    @Query("select * from players")
    fun getPlayers(): LiveData<List<PlayerDb>>

    @Delete
    suspend fun deletePlayer(player: PlayerDb): Int

}