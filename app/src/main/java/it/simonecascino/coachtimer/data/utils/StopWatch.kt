package it.simonecascino.coachtimer.data.utils

import java.lang.Exception
import java.util.*


class StopWatch : Timer(){

    var tick: (String) -> Unit = {}

    var time = 0L
        private set

    var isStarted = false
        private set

    private lateinit var task: TimerTask

    private fun createTaskInstance() = object: TimerTask(){

        override fun run() {

            time +=10

            val minutes = time/60000

            val unitsUnderMinutes = minutes*60000

            val seconds = (time - unitsUnderMinutes)/1000

            val cent = (time - unitsUnderMinutes - seconds*1000)/10

            val text = String.format("%02d:%02d.%02d", minutes, seconds, cent)

            tick(text)

        }

    }

    fun start(){

        if(isStarted)
            return

        task =  createTaskInstance()
        scheduleAtFixedRate(task, 0, 10)
        isStarted = true
    }

    fun stop(){

        if(!isStarted)
            return

        task.cancel()
        isStarted = false
    }

    fun destroy(){

        try{
            task.cancel()

        }catch(e: Exception){
            e.printStackTrace()
        } finally {
            cancel()
            purge()
        }

    }


}