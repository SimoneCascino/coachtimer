package it.simonecascino.coachtimer.data.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import it.simonecascino.coachtimer.data.database.daos.LapDao
import it.simonecascino.coachtimer.data.database.entities.LapDb
import it.simonecascino.coachtimer.domain.models.Lap

class LapsMediator(private val lapDao: LapDao): MediatorLiveData<List<Lap>>() {

    private var oldSession = 0L

    private lateinit var liveLaps: LiveData<List<LapDb>>

    fun connectToSession(liveSessionId: LiveData<Long>){

        addSource(liveSessionId){ session ->

            if(oldSession != session){

                oldSession = session

                if(::liveLaps.isInitialized)
                    removeSource(liveLaps)

                liveLaps = lapDao.getLapsBySessionId(session)

                addSource(liveLaps){

                    val laps = it.map { it.toDomainModel() }

                    if(value != laps)
                        value = laps

                }

            }

        }

    }

}